package com.gitlab.johnjvester.javadebugging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaDebuggingApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaDebuggingApplication.class, args);
	}

}
