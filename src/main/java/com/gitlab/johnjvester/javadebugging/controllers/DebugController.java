package com.gitlab.johnjvester.javadebugging.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.johnjvester.javadebugging.models.DebugDto;
import com.gitlab.johnjvester.javadebugging.services.DebugService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@CrossOrigin
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class DebugController {
	private final DebugService debugService;
	
	@GetMapping(value = "/debugs")
    public ResponseEntity<List<DebugDto>> getDebugList(@RequestParam int size, @RequestParam BigDecimal multiplier) {
        log.info("getDebugList( size={} multiplier={} )", size, multiplier);
        
        try {
            return new ResponseEntity<>(debugService.getDebugList(size, multiplier), HttpStatus.OK);
        } catch (NullPointerException | IllegalArgumentException e) {
        	return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
