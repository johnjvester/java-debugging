package com.gitlab.johnjvester.javadebugging.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.gitlab.johnjvester.javadebugging.models.DebugDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class DataUtils {
	private DataUtils() { }
	
	public static List<DebugDto> createData(int size) {
		List<DebugDto> list = new ArrayList<>();
		
		for (int i = 0; i < size; i++) {
			DebugDto debug = new DebugDto();
			debug.setId(i);
			debug.setName(UUID.randomUUID().toString());
			debug.setValue(new BigDecimal(BigInteger.valueOf(new Random().nextInt(100001)), 2));
			log.debug("debug={}", debug);
			
			list.add(debug);
		}
		
		return list;
	}

}
