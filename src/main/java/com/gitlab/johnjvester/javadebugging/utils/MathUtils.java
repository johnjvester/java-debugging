package com.gitlab.johnjvester.javadebugging.utils;

import java.math.BigDecimal;

import com.gitlab.johnjvester.javadebugging.models.DebugDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class MathUtils {
	private MathUtils() { }
	
	public static BigDecimal multiply(BigDecimal sourceValue, BigDecimal multiplier) {
		log.debug("multiply( sourceValue={} multiplier={} )", sourceValue, multiplier);
		
		BigDecimal result = sourceValue.multiply(multiplier);
		
		log.debug("{} x {} = {}", sourceValue, multiplier, result);
		return result;
	}
	
	/*
	 * This method intentionally introduces a bug by using the `result` property to 
	 * update the value of the `result` itself and is for example purposes.
	 */
	public static void multiply(DebugDto debug, BigDecimal multiplier) {
		log.debug("multiply( debug={} multiplier={} )", debug, multiplier);
		debug.setResult(multiply(debug.getResult(), multiplier));
	}

}
