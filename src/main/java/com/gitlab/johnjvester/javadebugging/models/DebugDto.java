package com.gitlab.johnjvester.javadebugging.models;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DebugDto {
	private int id;
	private String name;
	private BigDecimal value;
	private BigDecimal result;	
}
