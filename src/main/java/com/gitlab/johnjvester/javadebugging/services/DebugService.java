package com.gitlab.johnjvester.javadebugging.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.gitlab.johnjvester.javadebugging.models.DebugDto;
import com.gitlab.johnjvester.javadebugging.utils.DataUtils;
import com.gitlab.johnjvester.javadebugging.utils.MathUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DebugService {
	public List<DebugDto> getDebugList(int size, BigDecimal multiplier) throws Exception {
		log.info("getDebugList( size={} multiplier={} )", size, multiplier);
		
		if (size <= 0) {
			throw new IllegalArgumentException("size (" + size + ") must be greater than zero.");
		}
		
		if (multiplier == null) {
			throw new NullPointerException("multipler cannot be null");
		}
		
		List<DebugDto> debugList = DataUtils.createData(size);
		
		if (CollectionUtils.isNotEmpty(debugList)) {
			log.debug("Processing {} debug results", debugList.size());
			
			for (DebugDto debug : debugList) {
				debug.setResult(MathUtils.multiply(debug.getValue(), multiplier));
			}
		}
		
		log.info("debugList={}", debugList);
		return sanitizeResults(debugList, multiplier);
	}
	
	/*
	 * This method is to demonstrate a case where the `result` property is (incorrectly) 
	 * computed a second time.  To illustrate the issue, remove the comment syntax around the 
	 * call to `MathUtils.multiply()` in this method.
	 */
	private List<DebugDto> sanitizeResults(List<DebugDto> debugList, BigDecimal multiplier) {
		log.debug("sanitizeResults( debugList={} )", debugList);
		
		List<DebugDto> sanitizedList = new ArrayList<>();
		
		if (CollectionUtils.isNotEmpty(debugList)) {
			for (DebugDto debug : debugList) {
				// MathUtils.multiply(debug, multiplier);
				sanitizedList.add(debug);
			}
		}
		
		
		return sanitizedList;
	}

}
