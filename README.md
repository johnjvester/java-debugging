# Java Debugging (`java-debugging`) Spring Boot Example Application 

[![pipeline status](https://gitlab.com/johnjvester/java-debugging/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/java-debugging/commits/master)

> The `java-debugging` repository is a [Java](https://en.wikipedia.org/wiki/Java_(programming_language)) API service (utilizing the [Spring Boot](https://spring.io/projects/spring-boot) framework) to illustrate application debugging using an [Eclipse](https://www.eclipse.org) integrated development environment (IDE).

## Publications

To read publications related to my GitLab repositories, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## About the `java-debugging` RESTful Service

The `java-debugging` RESTful service will communicate one simple object, called `DebugDto`:

```
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DebugDto {
	private int id;
	private String name;
	private BigDecimal value;
	private BigDecimal result;
}
```

The `id` and `name` attributes exist for example purposes only and the `value` attribute will represent a random `BigDecimal` object which is established when a new `DebugDto` is created.

The `result` property is populated by a service class, called `DebugService`.  The `DebugService` performs validation of the input parameters, initializes a `List<DebugDto>` and also sets the `result` property on each `DebugDto` object.  For this example, the `result` property contains the result of multiplying the `DebugDto.value` by the provided `multiplier` (BigDecimal) object.  Keep in mind, this is a simple example.

## Using the `java-debugging` RESTful service

The `java-debugging` RESTful service contains a single URI:

`GET /debugs?size=3&multplier=1.25`

For simplicity the following `@RequestParam` are expected:

* `size` is the number of `DebugDto` objects to initialize
* `multiplier` is the multiplier value which will be used by the `DebugService`

Running the URI above returns a result set similar to what is displayed below:

```
[
    {
        "id": 0,
        "name": "35636f34-d42e-43f0-a97e-60c6de64375e",
        "value": 423.20,
        "result": 529.0000
    },
    {
        "id": 1,
        "name": "1f2eec6e-3af1-44e4-a2d0-8e80b9d1448a",
        "value": 782.72,
        "result": 978.4000
    },
    {
        "id": 2,
        "name": "4cec4dcb-c5b6-4687-b524-1f862ce43c2b",
        "value": 163.72,
        "result": 204.6500
    }
]
```

The first `DebugDto` item (`id` equals zero) contains a `value` of 423.20.  With a `multiplier` of 1.25, the `result` is being correctly set at 529.

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.

